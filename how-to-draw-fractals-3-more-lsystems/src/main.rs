// A slightly more complete L-System renderer in Rust.
mod fractal_def;
mod turtle;

use fractal_def::FractalDefinition;
use std::env;
use std::fs::File;
use std::io::BufReader;
use turtle::Turtle;

fn expand_instructions(fractal: &FractalDefinition) -> String {
    // Start with our initial instructions - the 'axiom':
    let mut instructions = fractal.axiom.clone();

    for _i in 0..fractal.iterations {
        let mut buffer = String::new();

        // For every character - if there is a rule for that character
        // replace that character with the rule. If there is no rule
        // simply leave the character alone:
        for c in instructions.chars() {
            if fractal.rules.contains_key(&c) {
                buffer.push_str(&fractal.rules[&c]);
            } else {
                buffer.push(c);
            }
        }

        // Set our instructions to what we just generated:
        instructions = buffer;
    }

    instructions
}

fn execute_instructions(turtle: &mut Turtle, fractal: &FractalDefinition, instructions: String) {
    turtle.init(fractal.start_x, fractal.start_y, fractal.start_angle);

    // This runs over our instruction string and executes each instruction:
    for c in instructions.chars() {
        match c {
            'F' => turtle.forward(fractal.forward),
            'G' => turtle.forward(fractal.forward),
            '+' => turtle.left(fractal.angle),
            '-' => turtle.right(fractal.angle),
            _ => {
                // Ignore - we allow characters that aren't drawing commands.
            }
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() == 1 {
        println!("Please supply a fractal definition file");
        return;
    }

    let json_file = File::open(&args[1]).expect("Couldn't open fractal definition file");
    let reader = BufReader::new(json_file);
    let fractal: FractalDefinition =
        serde_json::from_reader(reader).expect("Couldn't parse fractal definition");

    let width: u32 = fractal.width.unwrap_or(800);
    let height: u32 = fractal.height.unwrap_or(800);
    let mut t = Turtle::new(width, height);

    let instructions = expand_instructions(&fractal);
    execute_instructions(&mut t, &fractal, instructions);
    t.save();
}
