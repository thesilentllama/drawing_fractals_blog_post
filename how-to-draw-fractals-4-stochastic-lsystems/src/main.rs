// L-System renderer in Rust
mod fractal_def;
mod turtle;

use fractal_def::{FractalDefinition, Rules};
use rand::prelude::*;
use rand::rngs::StdRng;
use std::env;
use std::fs::File;
use std::io::BufReader;
use turtle::Turtle;

fn expand_instructions(fractal: &FractalDefinition) -> String {
    // Start with our initial instructions - the 'axiom':
    let mut instructions = fractal.axiom.clone();

    for _i in 0..fractal.iterations {
        let mut buffer = String::new();
        let mut rng = match fractal.seed {
            Some(seed) => StdRng::seed_from_u64(seed),
            None => StdRng::seed_from_u64(random()),
        };

        for c in instructions.chars() {
            match &fractal.rules {
                // We have two rule categories now:
                Rules::BasicRules(rules) => {
                    if rules.contains_key(&c) {
                        buffer.push_str(&rules[&c]);
                    } else {
                        buffer.push(c);
                    }
                },
                Rules::StochasticRules(rules) => {
                    if rules.contains_key(&c) {
                        for r in &rules[&c] {
                            let random_number: f32 = rng.gen();
                            // We generate a number between 0 and 1. Our test
                            // to see if we should use a rule is simple: if our
                            // random number is less than our probability then
                            // we apply the rule. 
                            if random_number < r.probability {
                                buffer.push_str(&r.rule);
                                break;
                            }
                        }
                    } else {
                        buffer.push(c);
                    }
                }
            }
        }

        instructions = buffer;
    }

    instructions
}

fn execute_instructions(turtle: &mut Turtle, fractal: &FractalDefinition, instructions: String) {
    turtle.init(fractal.start_x, fractal.start_y, fractal.start_angle);

    for c in instructions.chars() {
        match c {
            'F' => turtle.forward(fractal.forward),
            'G' => turtle.forward(fractal.forward),
            '+' => turtle.left(fractal.angle),
            '-' => turtle.right(fractal.angle),
            '[' => turtle.push(),
            ']' => turtle.pop(),
            _ => {
                // Ignore - we allow characters that aren't drawing commands.
            }
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() == 1 {
        println!("Please supply a fractal definition file");
        return;
    }

    let json_file = File::open(&args[1]).expect("Couldn't open fractal definition file");
    let reader = BufReader::new(json_file);
    let fractal: FractalDefinition =
        serde_json::from_reader(reader).expect("Couldn't parse fractal definition");

    let width: u32 = fractal.width.unwrap_or(800);
    let height: u32 = fractal.height.unwrap_or(800);
    let mut t = Turtle::new(width, height);

    let instructions = expand_instructions(&fractal);
    execute_instructions(&mut t, &fractal, instructions);
    t.save();
}
