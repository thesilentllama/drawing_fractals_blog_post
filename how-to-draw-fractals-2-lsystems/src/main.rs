// Drawing Fractals Part 2.
// This time round we implement a very basic L-system renderer.
mod turtle;
use turtle::Turtle;

fn expand_instructions(axiom: String, pattern: String, iterations: u16) -> String {
    // Start with our initial instructions - the 'axiom':
    let mut instructions = axiom;

    for _i in 0..iterations {
        let mut buffer = String::new();

        // Replace every 'F' instruction with the pattern:
        for c in instructions.chars() {
            if c == 'F' {
                buffer.push_str(&pattern);
            } else {
                buffer.push(c);
            }
        }

        // Set our instructions to what we just generated:
        instructions = buffer;
    }

    instructions
}

fn execute_instructions(turtle: &mut Turtle, instructions: String, steps: f32, angle: f32) {
    // This runs over our instruction string and executes each instruction:
    for c in instructions.chars() {
        match c {
            'F' => turtle.forward(steps),
            '+' => turtle.left(angle),
            '-' => turtle.right(angle),
            _ => {
                // Ignore anything else
            }
        }
    }
}

fn main() {
    let width: u32 = 1000;
    let height: u32 = 1000;
    let mut t = Turtle::new(width, height);

    t.init(90, 750, 0.0);
    let iterations = 5;
    execute_instructions(
        &mut t,
        expand_instructions("+F--F--F".to_string(), "F+F--F+F".to_string(), iterations),
        3.2,
        60.0,
    );
    t.save();
}
