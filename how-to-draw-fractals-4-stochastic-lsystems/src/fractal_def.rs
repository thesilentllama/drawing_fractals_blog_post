use serde::Deserialize;
use std::collections::HashMap;

#[derive(Deserialize)]
pub struct StochasticRule {
    pub probability: f32,
    pub rule: String,
}

#[derive(Deserialize)]
#[serde(untagged)]
pub enum Rules {
    BasicRules(HashMap<char, String>),
    StochasticRules(HashMap<char, Vec<StochasticRule>>)
}

#[derive(Deserialize)]
pub struct FractalDefinition {
    pub axiom: String,
    pub rules: Rules,
    pub iterations: u32,
    pub angle: f32,
    pub forward: f32,
    pub start_x: u32,
    pub start_y: u32,
    pub start_angle: f32,
    pub width: Option<u32>,
    pub height: Option<u32>,
    pub seed: Option<u64>,
}
