/// Mandelbrot Set renderer.
use image::{ImageBuffer, Rgb};
use num_complex::Complex64;
use std::path::Path;

fn main() {
    let width: u32 = 800;
    let height: u32 = 800;
    let aspect_ratio: f64 = width as f64 / height as f64;

    let mut imgbuf = ImageBuffer::from_pixel(width, height, Rgb([255, 255, 255]));
    let max_iterations: u64 = 20;
    let escape_threshold = 2.0;

    // See: http://www.cuug.ab.ca/dewara/mandelbrot/images.html for some zooms to
    // try render. Note: He gives a *centre* coordinate and radius. I've used a top
    // left point for the window so to convert make sure you subtract delta_x and
    // delta_y from the points on that page. Have fun :-)

    let delta_x = 3.0;
    let delta_y = delta_x / aspect_ratio;
    // This is the top left coordinate of the window:
    let start_x: f64 = -2.2;
    let start_y: f64 = -1.5;
    let step_x: f64 = delta_x / width as f64;
    let step_y: f64 = delta_y / height as f64;

    for x in 0..width {
        // Print a progress percentage:
        print!("{} %       \r", 100.0 * x as f32 / width as f32);
        for y in 0..height {
            let mut iteration = 0;
            let c = Complex64 {
                re: start_x + (x as f64 * step_x),
                im: start_y + (y as f64 * step_y),
            };

            let mut z = Complex64 { re: 0f64, im: 0f64 };

            while z.norm_sqr().sqrt() < escape_threshold && iteration < max_iterations {
                z = z * z + c;
                iteration += 1;
            }

            if iteration == max_iterations {
                imgbuf.put_pixel(x, y, image::Rgb([0, 0, 0]));
            }
        }
    }

    // Clears the progress percentage
    println!("\r              ");
    imgbuf
        .save(&Path::new("rendered.png"))
        .expect("Unable to save image");
}
