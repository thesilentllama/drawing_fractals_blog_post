/// Simple Koch Snowflake renderer
use image::{GenericImage, ImageBuffer, Rgb};
use imageproc::drawing::BresenhamLineIter;
use std::path::Path;

fn line(x0: i32, y0: i32, x1: i32, y1: i32, image: &mut impl GenericImage<Pixel = Rgb<u8>>) {
    // This returns each point on the line from (x0, y0) to (x1, y1). We simply colour in
    // a pixel at each point to draw the line:
    for (x, y) in BresenhamLineIter::new((x0 as f32, y0 as f32), (x1 as f32, y1 as f32)) {
        image.put_pixel(x as u32, y as u32, Rgb([150, 150, 150]));
    }
}

// Rusts math operations use radians internally - we'll need to
// convert our degrees into radians to use them. There is good
// reason for this: radians are generally more useful than degrees
// but degrees are better for our purposes since most of us
// are more used to them.
fn to_radians(angle: f32) -> f32 {
    (angle * std::f32::consts::PI) / 180.0
}

fn calculate_endpoint(x: i32, y: i32, angle: f32, length: f32) -> (i32, i32) {
    // We use the basic trigonometric identities to work out
    // where a line drawn at a given angle of a given length
    // will end. This follows from the definitions of sin and cos:
    // the sine function is the ratio of the opposite side and the hypotenuse
    // and the cosine function is the ratio of adjacent side and the hypotenuse.
    // Simply put sin(angle) = y/r and hence y = r * sin(angle)
    // Simarlarly cos(angle) = x/r and hence x = r * cos(angle).

    // Read dx as 'difference in the x direction' and
    // dy as 'difference in the y direction'.
    let dx = (length * (to_radians(angle)).cos()).round() as i32;
    let dy = (length * (to_radians(angle)).sin()).round() as i32;
    // The endpoint is calculated by taking our position where we started,
    // (x, y), and adding the difference on each axis that we just calculated.
    // Note: We *subtract* from y to move up. We need to do this because the
    // trigonometry functions we just used assume we started at the origin.
    (x + dx, y - dy)
}

fn draw_koch(
    imgbuf: &mut impl GenericImage<Pixel = Rgb<u8>>,
    iterations: u16,
    x: i32,
    y: i32,
    total_length: f32,
    angle: f32,
) {
    let side_length = total_length / 3.0;

    let (end_x, end_y) = calculate_endpoint(x, y, angle, side_length);
    let (end_x2, end_y2) = calculate_endpoint(end_x, end_y, angle, side_length);
    let (end_x3, end_y3) = calculate_endpoint(end_x2, end_y2, angle, side_length);
    let (midpoint_x, midpoint_y) = calculate_endpoint(end_x, end_y, angle + 60.0, side_length);

    if iterations == 0 {
        // Last iteration - do the actual drawing:
        line(x, y, end_x, end_y, imgbuf);
        line(end_x, end_y, midpoint_x, midpoint_y, imgbuf);
        line(midpoint_x, midpoint_y, end_x2, end_y2, imgbuf);
        line(end_x2, end_y2, end_x3, end_y3, imgbuf);
    } else {
        // The easy way to repeat a pattern - we simply call draw_koch again
        // every time we want to repeat. We pass in the coordinates and length
        // of the line we *would* have drawn - draw_koch will then replace it with
        // the pattern. Note that we decrease our iteration - we only do the
        // actual drawing when our iterations counter hits zero.
        draw_koch(imgbuf, iterations - 1, x, y, side_length, angle);
        draw_koch(
            imgbuf,
            iterations - 1,
            end_x,
            end_y,
            side_length,
            angle + 60.0,
        );
        draw_koch(
            imgbuf,
            iterations - 1,
            midpoint_x,
            midpoint_y,
            side_length,
            angle - 60.0,
        );
        draw_koch(imgbuf, iterations - 1, end_x2, end_y2, side_length, angle);
    }
}

fn main() {
    // We create a new image buffer - this will be our canvas, a grid of
    // pixels with width 800 and height 300 pixels. The default colour will
    // be white.
    let mut imgbuf = ImageBuffer::from_pixel(800, 300, Rgb([255, 255, 255]));

    // The starting data for our fractal:
    let iterations = 6;
    let fractal_length = 780.0;
    let start_angle = 0.0;
    let start_x = 10;
    let start_y = 250;

    // Draw the darn thing!
    draw_koch(
        &mut imgbuf,
        iterations,
        start_x,
        start_y,
        fractal_length,
        start_angle,
    );

    // Save the image to the file rendered.png. Quit if anything goes wrong
    // with an error message.
    imgbuf
        .save(&Path::new("rendered.png"))
        .expect("Unable to save image");
}
