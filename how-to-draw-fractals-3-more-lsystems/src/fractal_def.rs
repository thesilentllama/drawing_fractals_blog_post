use serde::Deserialize;
use std::collections::HashMap;

#[derive(Deserialize)]
pub struct FractalDefinition {
    pub axiom: String,
    pub rules: HashMap<char, String>,
    pub iterations: u32,
    pub angle: f32,
    pub forward: f32,
    pub start_x: u32,
    pub start_y: u32,
    pub start_angle: f32,
    pub width: Option<u32>,
    pub height: Option<u32>,
}
