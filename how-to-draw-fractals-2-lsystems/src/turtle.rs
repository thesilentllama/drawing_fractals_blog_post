// Turtle graphics implementation. This implements the bare minimum to
// get going. Real implementations have a lot more features.

use image::{GenericImage, Rgb, RgbImage};
use imageproc::drawing::BresenhamLineIter;
use std::path::Path;

fn to_radians(angle: f32) -> f32 {
    (angle * std::f32::consts::PI) / 180.0
}

fn line(image: &mut impl GenericImage<Pixel = Rgb<u8>>, x0: u32, y0: u32, x1: u32, y1: u32) {
    for (x, y) in BresenhamLineIter::new((x0 as f32, y0 as f32), (x1 as f32, y1 as f32)) {
        image.put_pixel(x as u32, y as u32, Rgb([150, 150, 150]));
    }
}

pub struct Turtle {
    pen: bool,
    x: u32,
    y: u32,
    angle: f32,
    canvas: RgbImage,
}

impl Turtle {
    pub fn new(width: u32, height: u32) -> Turtle {
        Turtle {
            pen: true,
            // By default place the turtle in the centre of the canvas:
            x: width / 2,
            y: height / 2,
            angle: 0.0,
            canvas: image::ImageBuffer::from_pixel(width, height, Rgb([255, 255, 255])),
        }
    }

    pub fn init(self: &mut Self, x: u32, y: u32, angle: f32) {
        self.x = x;
        self.y = y;
        self.angle = angle;
    }

    pub fn forward(self: &mut Self, steps: f32) {
        let dx = (steps * to_radians(self.angle).cos()).round() as i32;
        let dy = (steps * to_radians(self.angle).sin()).round() as i32;
        let x1 = (self.x as i32 + dx) as u32;
        let y1 = (self.y as i32 + dy) as u32;

        if self.pen {
            line(&mut self.canvas, self.x, self.y, x1, y1);
        }

        // Move the turtle to the new coordinate:
        self.x = x1;
        self.y = y1;
    }

    pub fn left(self: &mut Self, angle: f32) {
        self.angle -= angle;
    }

    pub fn right(self: &mut Self, angle: f32) {
        self.angle += angle;
    }

    pub fn save(self: &mut Self) {
        self.canvas
            .save(&Path::new("rendered.png"))
            .expect("Unable to save rendered png");
    }
}
