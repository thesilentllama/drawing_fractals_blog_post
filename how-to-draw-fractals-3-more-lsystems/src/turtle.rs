use image::{GenericImage, Rgb, RgbImage};
use imageproc::drawing::BresenhamLineIter;
use std::path::Path;

fn to_radians(angle: f32) -> f32 {
    (angle * std::f32::consts::PI) / 180.0
}

fn line(image: &mut impl GenericImage<Pixel = Rgb<u8>>, x0: u32, y0: u32, x1: u32, y1: u32) {
    for (x, y) in BresenhamLineIter::new((x0 as f32, y0 as f32), (x1 as f32, y1 as f32)) {
        image.put_pixel(x as u32, y as u32, Rgb([150, 150, 150]));
    }
}

pub struct Turtle {
    pub pen: bool,
    pub x: i32,
    pub y: i32,
    pub width: u32,
    pub height: u32,
    pub angle: f32,
    pub canvas: RgbImage,
}

impl Turtle {
    pub fn new(width: u32, height: u32) -> Turtle {
        Turtle {
            pen: true,
            x: (width / 2) as i32,
            y: (height / 2) as i32,
            width: width,
            height: height,
            angle: 270.0,
            canvas: image::ImageBuffer::from_pixel(width, height, Rgb([255, 255, 255])),
        }
    }

    pub fn init(self: &mut Self, x: u32, y: u32, angle: f32) {
        self.x = x as i32;
        self.y = y as i32;
        self.angle = angle;
    }

    pub fn forward(self: &mut Self, steps: f32) {
        let dx = (steps * to_radians(self.angle).cos()).round() as i32;
        let dy = (steps * to_radians(self.angle).sin()).round() as i32;
        let x1 = (self.x as i32 + dx) as u32;
        let y1 = (self.y as i32 + dy) as u32;
        let x0 = self.x as u32;
        let y0 = self.y as u32;

        // Don't draw anything if we're out of bounds:
        let mut out_of_bounds = false;

        if x0 >= self.width || x1 >= self.width {
            out_of_bounds = true;
        }

        if y0 >= self.height || y1 >= self.height {
            out_of_bounds = true;
        }

        if self.pen && !out_of_bounds {
            line(&mut self.canvas, x0, y0, x1, y1);
        }

        // Allow the turtle to move out of bounds - you never know, it might come back...
        self.x = x1 as i32;
        self.y = y1 as i32;
    }

    pub fn left(self: &mut Self, angle: f32) {
        self.angle -= angle;
    }

    pub fn right(self: &mut Self, angle: f32) {
        self.angle += angle;
    }

    pub fn save(self: &mut Self) {
        self.canvas
            .save(&Path::new("rendered.png"))
            .expect("Unable to save rendered png");
    }
}
